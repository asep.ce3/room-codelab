package com.kkp.roomwordsample.adapter

import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.kkp.roomwordsample.data.WordRoomDatabase
import kotlinx.coroutines.CoroutineScope
