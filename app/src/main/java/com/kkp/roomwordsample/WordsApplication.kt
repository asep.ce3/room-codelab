package com.kkp.roomwordsample

import android.app.Application
import com.kkp.roomwordsample.data.WordRepository
import com.kkp.roomwordsample.data.WordRoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class WordsApplication : Application() {
    val applicationScpe = CoroutineScope(SupervisorJob())
    val database by lazy { WordRoomDatabase.getDatabase(this, applicationScpe) }
    val repository by lazy { WordRepository(database.wordDao()) }
}